import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware} from 'redux';
import {Provider, ReactReduxContext} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import {forceReducerReload, createInjectorsEnhancer} from 'redux-injectors';
import {Route, Switch} from 'react-router-dom';

import {storeCreator, history, getStoreReducer} from "./utils";
import {AuthRoot} from './containers/Auth';

    const MOUNT_NODE = document.getElementById('root');

    const ref = {
    store: null,
};

    if (module.hot && module.hot.data && module.hot.data.store) {
    ref.store = module.hot.data.store;
    forceReducerReload(ref.store)
} else {
    const sagaMiddleware = createSagaMiddleware();

    const enhancers = [
        applyMiddleware(routerMiddleware(history), sagaMiddleware),
        createInjectorsEnhancer({createReducer: getStoreReducer, runSaga: sagaMiddleware.run}),
    ];

    ref.store = storeCreator({
        reducer: getStoreReducer(),
        middleware: [],
        enhancers,
    });

const key = 'app';

const App = () => {
    useInjectReducer({ key, reducer: appReducer });
    useInjectSaga({ key, saga: appSaga });

    return (
        <Switch>
            <Route path={['/sign-in', '/sign-up']} component={AuthRoot}/>
            <Route component={NotFoundPage} />
        </Switch>
    );
};

const ConnectedApp = () => {
    return (
        <Provider store={ref.store} context={ReactReduxContext}>
            <ConnectRouter history={history} context={ReactReduxContext}>
                <App />
            </ConnectRouter>
        </Provider>
    );
};
ReactDOM.render(<ConnectedApp />, MOUNT_NODE);

};




