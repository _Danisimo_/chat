import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles({
  messageArea: {
    height: '70vh',
    overflowY: 'auto',
  },
});

export const MessageList = (props) => {
  const { messages } = props;

  const classes = useStyles();

  return (
    <List className={classes.messageArea}>
      {messages
        ? messages.map((message, index) => {
            return (
              <ListItem key={`${index + 1}`}>
                <Grid container>
                  <Grid item xs={12}>
                    <ListItemText align="right" primary={message.message} />
                  </Grid>
                  <Grid item xs={12}>
                    <ListItemText
                      align="right"
                      secondary={String(new Date(message.date).toLocaleString())}
                    />
                  </Grid>
                </Grid>
              </ListItem>
            );
          })
        : null}
    </List>
  );
};
