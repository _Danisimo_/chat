import React, { useCallback, useState, useContext } from 'react';

import { Chat } from '../Chat/Chat';

export const ChatRoom = (props) => {
    const {rooms} = props;

    return (
        <div>
            <Chat rooms={rooms}/>
        </div>
    );
};
