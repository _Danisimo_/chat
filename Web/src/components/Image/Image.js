import React from "react";

const Image = ({ src, alt = "image", width = "100%", height = "initial" }) => {// alt - текст на случай, если картинка не загрузилась
return (                                                                       //"initial" - значение браузера по умолчанию
        <img src={src} alt={alt} style={{width, height}}/>
    );
}

export { Image }