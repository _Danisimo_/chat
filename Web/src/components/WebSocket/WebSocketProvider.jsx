import React, { createContext } from 'react';
import { useDispatch } from 'react-redux';
import io from 'socket.io-client';

import { WS_BASE } from '../../config.json';
import { updateChatLog, sendMessageRequest } from '../../actions';

export const WebSocketContext = createContext(null);

export const WebSocketProvider = ({ children }) => {
  let socket;
  let ws;

  const dispatch = useDispatch();

  const sendMessage = (roomId, message) => {
    const payload = {
      roomId,
      data: message,
    };

    socket.emit('event://send-message', JSON.stringify(payload));

    dispatch(sendMessageRequest(payload));
  };

  if (!socket) {
    socket = io.connect(WS_BASE);

    socket.on('event://get-message', (msg) => {
      const payload = JSON.parse(msg);
      dispatch(updateChatLog(payload));
    });

    ws = {
      socket,
      sendMessage,
    };
  }

  return <WebSocketContext.Provider value={ws}>{children}</WebSocketContext.Provider>;
};
