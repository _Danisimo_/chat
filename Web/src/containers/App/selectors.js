import { createSelector } from 'reselect';

import { initialState } from "./appReducer";

export const selectAppDomain = (state) => state.app || initialState;
export const selectRouter = (state) => state.router;

export const selectIsReady = createSelector(selectAppDomain, () => app.isReady);
export const selectCurrentUser = createSelector(selectAppDomain, () => app.currentUser);