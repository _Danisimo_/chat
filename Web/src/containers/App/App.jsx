import React from 'react';
import { Box } from '@material-ui/core';
import { Route, Switch } from 'react-router';

import { useInjectReducer, useInjectSaga } from 'redux-injectors';
import { Menu } from '../../components';
import { About, Conversations, People } from '../../layouts';
import { appSaga } from "./appSaga";
import { appReducer } from "./appReducer";

const key = 'app';

export const App = () => {
    useInjectReducer({ key, reducer: appReducer });
    useInjectSaga({ key, saga: appSaga });

    return(
        <Box>
            <Menu>
                <Switch>
                    <Route path="/about" component={About} />
                    <Route path="/community" component={Conversations} />
                    <Route path="/users" component={People} />
                </Switch>
            </Menu>
        </Box>
    );
};