import { put, takeLatest, all, call } from 'redux-saga/effects';
import axios from 'axios';
import { APP_START_INIT } from './constants';
import { actionUserRegisterSuccess } from "../Auth/actions";
import { push } from 'connected-react-router';
import {selectUserAuthData} from "../Auth/selectors";
import {actionUserGetSuccess} from "./actions";

function* handleGetUsers() {
    const requestUrl = 'https://nestjs-chat-api.herokuapp.com/api/users';

    try {
        return yield axios
            .post(requestUrl)
            .then(response => response.data)
            .catch((e) => console.error(e));
    } catch (e) {
        console.error(e);
    }
}

function* handleGetRooms() {
    const requestUrl = 'http://nestjs-chat-api.herokuapp.com/api/rooms';

    try {
        return yield axios
            .get(requestUrl)
            .then((response) => response.data)
            .catch((e) => console.error(e));
    } catch (e) {
        console.error(e);
    }
}

function* handleAppStartInit() {
    try {
        const { user } = yield select(selectUserAuthData);

        const [currentUser, users, rooms] = yield all([
            call(handleGetCurrentUser, user._id),
            call(handleGetUsers),
            call(handleGetRooms),
        ]);

        yield put(actionUserGetSuccess({ currentUser }));
        yield put(actionUsersGetSuccess({ users }));
        yield put(actionRoomsGetSuccess({ rooms }));
    } catch (e) {
        console.error(e);
    }
}

export function* appSaga() {
    yield takeLatest(APP_START_INIT, handleAppStartInit);
}