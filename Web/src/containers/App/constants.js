export const APP_START_INIT = 'APP_START_INIT';
export const APP_FINISH_INIT = 'APP_FINISH_INIT';

export const USER_GET_REQUEST = 'USER_GET_REQUEST';
export const USER_GET_SUCCESS = 'USER_GET_SUCCESS';
export const USER_GET_ERROR = 'USER_GET_ERROR';