import {
    APP_START_INIT,
    APP_FINISH_INIT,
    USER_GET_REQUEST,
    USER_GET_SUCCESS,
    USER_GET_ERROR
} from './constants';

export const actionAppStartInit = () => ({type: APP_START_INIT});
export const actionAppFinishInit = () => ({type: APP_FINISH_INIT});

export const actionUserGetRequest = () => ({type: USER_GET_REQUEST});
export const actionUserGetSuccess = () => ({type: USER_GET_SUCCESS});
export const actionUserGetError = () => ({type: USER_GET_ERROR});
