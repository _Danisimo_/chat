import { USER_AUTH_REQUEST, USER_AUTH_ERROR, USER_IS_AUTH_SET} from './constants';

export const actionUserAuthGet = (payload) => ({ type: USER_AUTH_REQUEST, payload });
export const actionUserAuthError = () => ({ type: USER_AUTH_ERROR });
export const actionUserIsAuthSet = (payload) => ({ type: USER_IS_AUTH_SET, payload });

export const actionUserRegisterRequest = (payload) => ({ type: USER_AUTH_REQUEST, payload });
export const actionUserRegisterSuccess = () => ({ type: USER_AUTH_REQUEST });
export const actionUserRegisterErroe = () => ({ type: USER_AUTH_REQUEST });