import React from 'react';
import { Route, Switch } from 'react-router';
import { useInjectReducer, useIngectSaga } from 'redux-injectors';

import {SignIn, SignUp} from "./components";
import { authSaga } from "./authSaga";
import { authReducer } from './authReducer';
import { AuthRoute } from "../../router/AuthRoute";

const key = 'auth';

export const AuthRoot = () => {
    useIngectSaga({ key, saga: authSaga });
    useInjectReducer({ key, reducer: authReducer });

    return (
        <Switch>
            <AuthRoute
                exact
                path="/sign-up"
                useAuthBase={useAuthBase}
                actionAppStartInit={actionAppStartInit}
                component={SignUp}
            />
            <AuthRoute
                exact
                path="/sign-in"
                useAuthBase={useAuthBase}
                actionAppStartInit={actionAppStartInit}
                component={SignIn}
            />
        </Switch>
    );
};