import {USER_AUTH_ERROR, USER_IS_AUTH_SET} from './constants';

export const initialState = {
    isAuth: false,
    isAuthError: false,
    userAuthData: null,
}

export const AuthReducer = (state, action) => {
    const {type, payload} = action;

    switch (type) {
        case USER_IS_AUTH_SET: {
            const {isAuth, userAuthData} = payload;

            return {...state, isAuth, userAuthData};
        }
        case USER_AUTH_ERROR: {
            const {isAuth} = payload;
            return {...state, isAuth}
        }

        default: {
            return state;
        }
    }

};