import {takeLatest} from 'redux-saga/effects';
//import axios

import {USER_AUTH_REQUEST, USER_REGISTER_REQUEST} from "./constants";
//import

// function* getAuth(action) {
//     const {payload} = action
//     const requestUrl = 'http://restjs-chat-api.herokuapp.com/api/auth/login';
//
//     try {
//         yield axios
//             .post(requestUrl, payload)
//             .then(responce => responce.data)
//             .catch((e) => console.error(e));
//     } catch (e) {
//         console.error(e);
//     }
//
// }
//
// function* handleRegister(action) {
//     const {payload}
// }

export function* authSaga() {
    yield takeLatest(USER_AUTH_REQUEST, handleAuth);
    yield takeLatest(USER_REGISTER_REQUEST, handleRegister);
}