import React from 'react';
import { Link } from 'react-router-dom';
import {
  Divider,
  Box,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
} from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';

import { BadgeAvatar } from '../../../../components';
import config from '../../../../config.json';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  list: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export const People = (props) => {
  const {
    match: { path },
  } = props;

  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <List className={classes.list}>
        {config.users.map((user, index) => {
          return (
            <Link key={`${index + 1}`} to={`${path}/${String(user.name).toLowerCase()}`}>
              <ListItem>
                <ListItemAvatar>
                  <BadgeAvatar alt="Avatar" src={AccountCircle} isOnline={user.isOnline} />
                </ListItemAvatar>
                <ListItemText primary={user.name} />
              </ListItem>
              <Divider />
            </Link>
          );
        })}
      </List>
      <h2>People</h2>
    </Box>
  );
};
