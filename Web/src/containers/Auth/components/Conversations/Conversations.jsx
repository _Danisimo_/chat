import React, { useState } from 'react';
import { Box } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';

import { Chat } from '../../../../components';


export const Conversations = () => {
  const dispatch = useDispatch();
  // const [roomName, setRoomName] = useState('');
  const [roomId, setRoomId] = useState('');
  const rooms = useSelector((state) => state.rooms);
  const messages = useSelector((state) => state.messages);

  return (
    <Box>
      <Chat
        rooms={rooms.filter((room) => !room.is_user)}
        messages={messages[roomId]}
        handleChangeRoom={setRoomId}
      />
    </Box>
  );
};
