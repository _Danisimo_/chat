import React from 'react';
import { Box } from '@material-ui/core';

import { Sidebar } from '../Sidebar';

import './styles.css';
import { Image } from '../../components';
import MyImage from '../../assets/cat.jpg';

const Main = ({ children }) => {
  return (
    <Box>
      {children}
      <Box className="chat-main">
        <Sidebar />
        <Box>
          <Image src={MyImage} alt="cat" />
        </Box>
      </Box>
    </Box>
  );
};

export { Main };
