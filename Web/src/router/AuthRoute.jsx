import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';
import { useDispatch } from 'reacr-redux';

import { RenderComponent } from "./RenderComponent";

export const AuthRoute = (props) => {
    const {
        redirectPath = '/people',
        redirectOnLoggerIn = true,
        useAuthBase,
        actionAppStartInit,
        component,
        ...rest
    } = props;

    // const dispatch = useDispatch();
    //
    // const { isReady, isAuth, currentUser, Spinner } = useAuthBase();
    //
    // if (!isReady && isEmpty(currentUser) && isAuth) {
    //     dispatch(actionAppStartInit);
    // }

    const isLoading = !isReady && isAuth;
        const isRedirect = isReady && isAuth && redirectOnLoggerIn;

    // return (
    //    //
    // )

};

AuthRoute.propTypes = {
    path: PropTypes.string.isRequired,
    redirectPath: PropTypes.string,
    exact: PropTypes.bool,
    redirectOnLoggerIn: PropTypes.bool,
    useAuthBase: PropTypes.func,
    actionAppStartInit: PropTypes.func,
    component: PropTypes.any,
};