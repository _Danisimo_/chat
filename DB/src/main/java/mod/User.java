package mod;

import java.util.ArrayList;

public class User {

    private String firstName;
    private String surName;
    private String login;
    private String password;
    private int id;
    private ArrayList<Integer> roomsId;
    private String photoUrl;
    private boolean statusOnline;

    public User(int id, String firstName, String surName, String login, String password, ArrayList<Integer> roomsId, String photoUrl , boolean statusOnline) {
        this.id = id;
        this.firstName = firstName;
        this.surName = surName;
        this.login = login;
        this.password = password;
        this.roomsId = roomsId;
        this.photoUrl = photoUrl;
        this.statusOnline = statusOnline;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Integer> getRoomsId() {
        return roomsId;
    }

    public void setRoomsId(ArrayList<Integer> roomsId) {
        this.roomsId = roomsId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isStatusOnline() {
        return statusOnline;
    }

    public void setStatusOnline(boolean statusOnline) {
        this.statusOnline = statusOnline;
    }

}
