package dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.ArrayList;

public class Dao {

    private static Statement stmt;
    private static PreparedStatement preparedStatement;
    private static ResultSet rs;
    private static Connection connection = null;

    public static void PostgreRepository() {
        System.out.println("-------- PostgreSQL "
                + "JDBC Connection Testing ------------");
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {
            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();
        }
        System.out.println("PostgreSQL JDBC Driver Registered!");
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://ec2-3-248-4-172.eu-west-1.compute.amazonaws.com:5432/d53knvbfufupa3", "gnqzinvidwoiqa",
                    "3b966ca1a99d7a2f0e97131be7bd08862b343ef02467ced66ad98e855076db34");
            stmt = connection.createStatement();
        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }

        try {
            String myTableName = "CREATE TABLE users("
                    + "id   SERIAL PRIMARY KEY,"
                    + "firstName CHARACTER VARYING(30)     NOT NULL,"
                    + "surName CHARACTER VARYING(30) NOT NULL,"
                    + "login CHARACTER VARYING(30) NOT NULL,"
                    + "password CHARACTER VARYING(30) NOT NULL,"
                    + "roomsId CHARACTER VARYING(30) NOT NULL,"
                    + "photoUrl CHARACTER VARYING(30) NOT NULL,"
                    + "statusOnline CHARACTER VARYING(30) NOT NULL;";
            stmt.executeUpdate(myTableName);
            System.out.println("Table created");
        } catch (SQLException throwables) {

        }
    }
}

