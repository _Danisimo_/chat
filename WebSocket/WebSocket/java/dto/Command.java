package dto;

public enum Command {
    SENDMESSAGE,
    LOGIN,
    LOGOUT,
    CREATECHAT,
    REMOVECHAT,
    REGISTERUSER,
    STARTGAME,
    STOPGAME;
}
